"use strict";

var nodeFetch = require("node-fetch");
var Promise = require("bluebird");
nodeFetch.Promise = Promise;

module.exports = nodeFetch;
