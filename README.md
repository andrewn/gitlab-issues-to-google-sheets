# gitlab-issues-to-google-sheets

Query GitLab issues and send the results to a Google sheets

## Usage

```
Typical Example

  gitlab-issues-to-sheet --project "gitlab-com/gl-infra/production" --sheet-id '1b19MOslsGe-TYbpuVWG2O4mW1Y8brc-oZ_pRcQoaSug' --sheet-range 'Issues!A:Z'

Options

  --column string[]      A repeatable column definition.
                         Format is: <column_name>:<column_type>:<column_config>

                         Types can be:
                         * field: value of the <column_config> field
                         * scoped_label: value of the <column_config> scoped label
                         * regexp_label: first label to match the regexp <column_config>
                         * label: returns label when label <column_config> is applied
                         * labels: all labels, concatenated
                         * assignees: all assignees, concatenated
                         * date: format date column. column_field/format/truncate_to

  --project string
  --sheet-id string
  --sheet-range string
  --query string[]       Repeatable name=value pairs, passed to the GitLab Issues API
  ```

### Column Definition Examples

| Definition | Description |
|------------|-------------|
| `--column iid` | Emit column `iid` as the `iid` attribute of an issue |
| `--column created_at:date` | Emit column `created_as` as the `created_at` attribute of an issue, date formatted for Sheets |
| `--column created_at_week:date:created_at//week` | Column `created_at_week` as the `created_at` attribute, trucated to week |
| `--column incident_scoped:scoped_label:Incident` | Column `incident_scoped` as the value from the scoped label `Incident::*` |
| `--column severity:regexp_label:^S.$` | Column `severity` as the first label to match the regular expression `^S.$` (eg `S1`, `S2`, `S3` etc) |
| `--column labels:labels` | Column `labels` as all labels, comma-delimited |
| `--column assignees:assignees` | Column `assignees` as all assignees, comma-delimited |

## Environment Variables

This tool relies on the following environment variables

* `GITLAB_TOKEN`: token to use for accessing `gitlab.com`
* `OPS_GITLAB_TOKEN`: token to use for accessing `ops.gitlab.net`
* `GOOGLE_CREDS_FILE`: path to a file containing a Google authentication key

