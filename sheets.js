"use strict";

const { google } = require("googleapis");
const { auth } = require("google-auth-library");
var Promise = require("bluebird");
var fs = Promise.promisifyAll(require("fs"));

async function replaceSheet(spreadsheetId, range, rows) {
  if (!process.env.GOOGLE_CREDS_FILE) {
    throw new Error("Please configure GOOGLE_CREDS_FILE environment variable to point to JWT token");
  }
  let sheets = google.sheets("v4");

  const key = await fs.readFileAsync(process.env.GOOGLE_CREDS_FILE);
  const client = auth.fromJSON(JSON.parse(key));
  client.scopes = ["https://www.googleapis.com/auth/spreadsheets"];

  let request = {
    spreadsheetId: spreadsheetId,
    range: range,
    valueInputOption: "USER_ENTERED",
    resource: {
      range: range,
      values: rows,
    },
    auth: client,
  };

  await sheets.spreadsheets.values.update(request);
}

module.exports = replaceSheet;
