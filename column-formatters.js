#!/usr/bin/env node
"use strict";

const moment = require("moment");

function fieldFormatter(fieldName) {
  return (issue) => issue[fieldName];
}
fieldFormatter.description = "value of the <column_config> field";

function labelsFormatter() {
  return (issue) => issue.labels.join(",");
}
labelsFormatter.description = "all labels, concatenated";

function scopedLabelFormatter(scopedLabelName) {
  return (issue) => {
    let first = issue.labels.find((label) => label.startsWith(`${scopedLabelName}::`));
    if (!first) return "";
    return first.split("::")[1];
  };
}
scopedLabelFormatter.description = "value of the <column_config> scoped label";

function regexMatchLabelFormatter(matcherString) {
  let matcher = new RegExp(matcherString);

  return (issue) => {
    let first = issue.labels.find((label) => matcher.test(label));
    return first || "";
  };
}
regexMatchLabelFormatter.description = "first label to match the regexp <column_config>";

function matchLabelFormatter(matcher) {
  return (issue) => {
    let first = issue.labels.find((label) => matcher == label);
    return first || "";
  };
}
matchLabelFormatter.description = "returns label when label <column_config> is applied";

function assigneesFormatter() {
  return (issue) => issue.assignees.map((assignee) => assignee.username).join(", ");
}
assigneesFormatter.description = "all assignees, concatenated";

function dateFormatter(conf) {
  let [fieldName, formatterString, startOfString] = conf.split("/");

  let parser = (issue) => moment(issue);
  return (issue) => {
    let v = issue[fieldName];
    if (!v) return "";
    let d = moment(v);
    if (startOfString) {
      d = d.startOf(startOfString);
    }
    return d.format(formatterString || "YYYY-MM-DD HH:mm:ss");
  };
}
dateFormatter.description = "format date column. column_field/format/truncate_to";

module.exports = {
  field: fieldFormatter,
  scoped_label: scopedLabelFormatter,
  regexp_label: regexMatchLabelFormatter,
  label: matchLabelFormatter,
  labels: labelsFormatter,
  assignees: assigneesFormatter,
  date: dateFormatter,
};
